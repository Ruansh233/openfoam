    const word dictName("mergeMultipleMeshesDict");
    #include "setSystemMeshDictionaryIO.H"
    Info<< "Reading " << dictIO.instance()/dictIO.name() << nl << endl;
    const IOdictionary mergeMultipleMeshesDict(dictIO);

    fileName masterCase(mergeMultipleMeshesDict.lookup("masterCase"));
    const List<fileName> addCaseListTmp(mergeMultipleMeshesDict.lookup("addCase"));    
    List<fileName> addCaseList;
    const List<fileName> foldersList(readDir(mesh.time().rootPath(), fileName::DIRECTORY));
    wordRe fileNameRex;      

    forAll(addCaseListTmp, listI)
    {
        fileNameRex = addCaseListTmp[listI];
        fileNameRex.compile();
        
        if(fileNameRex.isPattern())
        { 
            forAll(foldersList, folder)
            {
                if(fileNameRex.match(foldersList[folder]))
                    addCaseList.append(mesh.time().rootPath() + "/" + foldersList[folder]);
            }
        }
        else
        {
            addCaseList.append(addCaseListTmp[listI]);
        }
    }

    SortableList <fileName> addCaseListSorted(addCaseList);
    addCaseListSorted.sort();

